import Player from "../Character/Player";
import { game } from "../game";

export class Movimiento {

    player: Player;
    teclas: any;
    escenario: Phaser.Scene;


    direccion: Phaser.Math.Vector2;


    constructor(player: Player) {
        this.player = player;
        this.escenario = this.player.escenario;
        this.direccion = new Phaser.Math.Vector2(0, 0);


        this.teclas = {
            izquierda: this.escenario.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT),
            derecha: this.escenario.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT),
            abajo: this.escenario.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN),
            arriba: this.escenario.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP)
        }
        this.escenario.input.keyboard.on('keydown_W', (e) => {
            this.direccion.y = -1;
            this.player.body.setVelocityY(this.direccion.y * this.player.velocidad);
            this.player.setTexture('player_run');
            this.player.anims.play('run');
        })
        this.escenario.input.keyboard.on('keyup_W', (e) => {
            this.player.body.setVelocityY(0)
        })

        this.escenario.input.keyboard.on('keydown_S', (e) => {
            this.direccion.y = 1;
            this.player.body.setVelocityY(this.direccion.y * this.player.velocidad);            
        })
        this.escenario.input.keyboard.on('keyup_S', (e) => {
            this.player.body.setVelocityY(0)
        })
        this.escenario.input.keyboard.on('keydown_A', (e) => {
            this.direccion.x = -1;
            this.player.body.setVelocityX(this.direccion.x * this.player.velocidad);
        })
        this.escenario.input.keyboard.on('keyup_A', (e) => {
            this.player.body.setVelocityX(0)
        })
        this.escenario.input.keyboard.on('keydown_D', (e) => {
            this.direccion.x = 1;
            this.player.body.setVelocityX(this.direccion.x * this.player.velocidad);
        })
        this.escenario.input.keyboard.on('keyup_D', (e) => {

            this.player.body.setVelocityX(0)

        })
        // Locks pointer on mousedown
        game.canvas.addEventListener('mousedown', () => {
            game.input.mouse.requestPointerLock();
        });
        // Exit pointer lock when Q or escape (by default) is pressed.
        this.escenario.input.keyboard.on('keydown_Q', (event) => {
            if (game.input.mouse.locked)
                game.input.mouse.releasePointerLock();
        }, 0);

        // Move reticle upon locked pointer move
        this.escenario.input.on('pointermove', (pointer) => {
            if (this.escenario.input.mouse.locked) {
                this.player.reticulo.x += pointer.movementX;
                this.player.reticulo.y += pointer.movementY;
            }
        }, this);

        this.escenario.input.on('pointerdown', (pointer) => {

            let poder = this.player.poderes.get();
            
            if (poder) {
                poder.setActive(true).setVisible(true);
                let velocity: Phaser.Math.Vector2 = new Phaser.Math.Vector2()
                this.escenario.physics.velocityFromRotation(this.player.rotation, this.player.velocidad, velocity);
                poder.fire(this.player, { x: velocity.x * 180 / Math.PI, y: velocity.y * 180 / Math.PI });
            }

        });        





    }







    rotarConMouse() {

        // this.player.setAngle(angle);
        this.player.rotation = Phaser.Math.Angle.Between(this.player.x, this.player.y, this.player.reticulo.x, this.player.reticulo.y);

        // Makes reticle move with player
        this.player.reticulo.setVelocityX(this.player.body.velocity.x);
        this.player.reticulo.setVelocityY(this.player.body.velocity.y);
        this.ajustarReticula();


    }
    ajustarReticula(reticle = this.player.reticulo) {
        let distX = reticle.x - this.player.x; // X distance between player & reticle
        let distY = reticle.y - this.player.y; // Y distance between player & reticle

        // Ensures reticle cannot be moved offscreen
        if (distX > 800)
            reticle.x = this.player.x + 800;
        else if (distX < -800)
            reticle.x = this.player.x - 800;

        if (distY > 450)
            reticle.y = this.player.y + 600;
        else if (distY < -450)
            reticle.y = this.player.y - 600;
    }




}

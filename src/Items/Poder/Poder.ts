import { Scene } from "phaser";
import Player from "../../Character/Player";

export default class Poder extends Phaser.GameObjects.Image{

    player: Player;
    damage: number;
    nombre: String;
    tipo: String;
    rango: number;
    energia: number;


    speed: number;
    born: number;
    direction: number;
    xSpeed: number;
    ySpeed: number;

    escenario: Scene;

    constructor(escenario: Scene, posX: number, posY: number, ) {
        // constructor(posX: number, posY: number, player: Player, config: {damage:number, nombre:String, tipo:String, rango:number}) {
        super(escenario, posX, posY, 'player');
        // this.player = player;
        this.escenario = escenario;
        
        this.speed = 0.5;
        this.born = 0;
        this.direction = 0;
        this.xSpeed = 0;
        this.ySpeed = 0;
        this.setOrigin(0.5, 0.5);
        this.setScale(0.009, 0.009);
        this.setSize( (this.scaleX * this.width), (this.scaleY * this.height));

        


    }
    fire(shooter: Phaser.GameObjects.Sprite, target: { x: number, y: number }) {

        this.setPosition(shooter.x, shooter.y); // Initial position
        this.direction = Math.atan((target.x - this.x) / (target.y - this.y));
        //some light randomness to the bullet angle
        this.direction += ((Math.random() / 10) + (-(Math.random() / 10)));

        // Calculate X and y velocity of bullet to moves it from shooter to target
        if (target.y >= this.y) {
            this.xSpeed = this.speed * Math.sin(this.direction);
            this.ySpeed = this.speed * Math.cos(this.direction);
        }
        else {
            this.xSpeed = -this.speed * Math.sin(this.direction);
            this.ySpeed = -this.speed * Math.cos(this.direction);
        }

        this.rotation = shooter.rotation; // angle bullet with shooters rotation
        this.born = 0;
    }

    update(time: any, delta: number) {
        this.x += this.xSpeed * delta;
        this.y += this.ySpeed * delta;
        this.born += delta;

        if (this.born > 1500) {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}


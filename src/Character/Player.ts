import { Movimiento } from "../Scripts/Movimiento";
import Poder from "../Items/Poder/Poder";
import { game } from "../game";

export default class Player extends Phaser.GameObjects.Sprite {


    //global player
    container: Phaser.GameObjects.Container;

    // config player
    teclas: any;

    // props player
    aceleracion: number;
    velocidad: number;

    acel_angular: number;
    vel_angular: number;

    max_acel: number;
    max_vel: number;

    max_acel_angular: number;
    max_vel_angular: number;



    body: Phaser.Physics.Arcade.Body;
    escenario: Phaser.Scene;
    movimiento: Movimiento;

    reticulo: any;
    poderes: any;

    player: any;


    constructor(scene: Phaser.Scene, posX: number, posY: number) {
        super(scene, posX, posY, 'player');

        //inicializar la escena
        this.escenario = scene;

        //inicializar las variables físicas para el movimiento y la rotación
        this.aceleracion = 100;
        this.velocidad = 100;
        this.acel_angular = 60;
        this.vel_angular = 100;
        this.max_acel = 200;
        this.max_vel = 200;
        this.max_acel_angular = 150;
        this.max_vel_angular = 190;

        
        //escalar el tamaño de los sprites
        this.setScale(0.0312);
        
        //asignar origen a los sprites
        this.setOrigin(0.5, 0.5)
        
        //inicializar el motor físico de los sprites
        this.escenario.physics.add.existing(this);                

        // Animaciones del player
        this.anims.animationManager.create({
            key: 'idle',
            frames: this.anims.animationManager.generateFrameNumbers('player', {
               start: 0, end: 15 
            }),
            frameRate: 15,
            repeat: -1
        });

        this.anims.animationManager.create({
            key: 'run',
            frames: this.anims.animationManager.generateFrameNumbers('player_run', {
               start: 0, end: 15 
            }),
            frameRate: 16,
            repeat: -1
        });
        
        this.setTexture('player');        
        this.anims.play('idle');


        //inicializar las propiedades físicas inherentes al player
        this.body.setSize(this.width, this.height);
        this.body.setMaxVelocity(this.max_vel);
        

        this.reticulo = this.scene.physics.add.sprite(20, 50, 'player');
        this.reticulo.setOrigin(0.5, 0.5).setDisplaySize(25, 25).setCollideWorldBounds(true);
        this.reticulo.setScale(0.01, 0.01);

        //asignar cámara principal al player
        this.escenario.cameras.main.startFollow(this);


        //configuraciones físicas con el mapa de los sprites
        this.body.setCollideWorldBounds(true);

        
        //cargar módulo de movimientos
        this.movimiento = new Movimiento(this);


        //mostrar los sprites en la escena 
        this.escenario.add.existing(this);


        //cargar los poderes en la escena
        this.poderes = this.escenario.physics.add.group({ classType: Poder, maxSize: 12, runChildUpdate: true });        

        // Stops player acceleration on uppress of WASD keys
        // this.scene.input.keyboard.on('keyup_W', (event) => {
        //     if (this.teclas.abajo.isUp)
        //         this.body.setAccelerationY(0)
        // }, this);







    }

    update() { 
        this.movimiento.rotarConMouse();         
    }
    


}
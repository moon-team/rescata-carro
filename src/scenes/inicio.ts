import Player from "../Character/Player.js";

export default class inicioClass extends Phaser.Scene {
  player: Player;
  player2: Player;
  cursors: any;
  bg: any;
  wall: any;
  platforms: any;
  score: any = 0;
  scoreText: any;
  layer: any;
  bullets: any;
  p:any;
  constructor() {
    super('inicioClass');
  }


  preload() {
    this.load.image('background', './assets/bgGame.png');
    this.load.image('wall', './assets/Wall.png');
    this.load.image('tilemap', './assets/murosTile.png');
    this.load.tilemapTiledJSON('map', 'assets/mapa/mapa2.json');    
    this.load.spritesheet("player","./assets/heroe_ide.png",{
      frameWidth: 511,
      frameHeight: 512,
    });
    this.load.spritesheet("player_run","./assets/heroe_run.png",{
      frameWidth: 511,
      frameHeight: 512,
    });    
    
  }

  create() {
    // Tile map
    const map = this.make.tilemap({ key: 'map' });
    // Se pasa el nombre que tiene el tileset en Tiled y luego el del tileset cargado en preload
    const tiles = map.addTilesetImage("murosTile", "tilemap");
    // Se pasa como primer parámetro el nombre creado de la malla en Tiled o su posición
    this.layer = map.createStaticLayer(0, tiles, 0, 0).setOrigin(0);
    // Selecciona de las imagenes por posiciones cuales harán colisión en este caso selecciona la imagen 1 y 2 del tileset
    map.setCollisionBetween(0, 6);    



    // this.p = this.physics.add.sprite(100, 40, 'player');
    // this.p.setScale(0.0312);
    // this.anims.create({
    //   key: 'player_anim',
    //   frames: this.anims.generateFrameNumbers('player', {
    //      start: 0, end: 15 
    //   }),
    //   frameRate: 15,
    //   repeat: -1
    // });


    // Score
    this.scoreText = this.add.text(20, 0, "🎮 Score " + this.score).setFontFamily('Arial').setFill("blue").setFontSize(20).setScrollFactor(0);


    this.player = new Player(this, 40, 40)



    // Cámara        
    this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.cameras.main.setZoom(3);
  }

  update() {
    // Activa colisión entre el player y el tilemap
    this.player.update();
    this.physics.collide(this.player, this.layer);

    //   // Colisión entre wall y player
    //   this.physics.add.collider(this.player, this.platforms, this.choque, null, this);
    //   this.bullets.children.each(function(b) {
    //     if (b.active) {
    //       if (b.y < 0) {
    //         b.setActive(false);          
    //     }
    //       }
    //   }.bind(this));
    // }    

    // choque = function(player, platforms){
    //   console.log("Asdasd");  
    //   //platforms.disableBody(true, true);
    // }
    // shoot(pointer) {
    //   var bullet = this.bullets.get(pointer.x, pointer.y);
    //   console.log(pointer.x, pointer.y);
    //   if (bullet) {
    //       bullet.setScale(0.01);
    //       bullet.setActive(true);
    //       bullet.setVisible(true);          
    //       bullet.body.velocity.y = -200;
    //   }
    // }
  }
}
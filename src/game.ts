import 'phaser';
import bootGame from './scenes/bootGame';
import inicioClass from './scenes/inicio';

const config = {
    type: Phaser.AUTO,
    backgroundColor: '#000000', //
    width: 800, 
    height: 450,
    parent: "game-container",
    pixelArt: true,
    // scale: {
    //     mode: Phaser.Scale.FIT,
    //     width: 600,
    //     height: 450,
    //     zoom: 2
    // },
    physics: {
        default: 'arcade', 
        arcade: {
            fps: 60,
            gravity: { y: 0 },
            debug:false
        }        
        
    },  
    scene: [bootGame,inicioClass]
};

export const game = new Phaser.Game(config);

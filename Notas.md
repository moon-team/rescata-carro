### Guias
https://phaser.io/examples
https://labs.phaser.io/index.html?dir=&q=
https://rexrainbow.github.io/phaser3-rex-notes/docs/site/camera/

### Template
https://github.com/photonstorm/phaser3-typescript-project-template

### Tile map JSON
https://github.com/mikewesthad/phaser-3-tilemap-blog-posts
https://labs.phaser.io/edit.html?src=src/bugs%5Cap%20tilemap.js
https://medium.com/@michaelwesthadley/modular-game-worlds-in-phaser-3-tilemaps-1-958fc7e6bbd6